package main

import (
	"fmt"
	"os"
	"slices"
	"strings"
	"time"

	"golang.org/x/sys/windows"
)

var debug bool = false
var x []string = []string{"o", "y", "a", "e", "i", "u"}
var processlist map[uint32]string
var processparentlist map[uint32]uint32
var interval1 int = 10
var interval2 int = 10
var lastrefresh time.Time
var getenv bool = false

func displayLimited(pid uint32, h windows.Handle) {
	s := "\n"
	s += fmt.Sprintf(strings.Repeat("-", 50) + "\n")

	if debug {
		s += fmt.Sprintf("HANDLE: LIMITED\n")
	}
	s += fmt.Sprintf("PID: %d\n", pid)

	chain, err := resolveProcessChain(pid)
	if err == nil {
		s += fmt.Sprintf("PARENTS: %s\n", chain)
	}

	exe, err := getExecutablePath(h)
	if err != nil {
		if debug {
			fmt.Printf("%v\n", err)
		}
		exe, err := getExecutable(pid)
		if err != nil {
			if debug {
				fmt.Printf("%v\n", err)
			}
		} else {
			s += fmt.Sprintf("EXE: %s\n", exe)
		}
	} else {
		s += fmt.Sprintf("PATH: %s\n", exe)
	}

	fmt.Print(s)
}

func display(pid uint32, h windows.Handle, infolevel int) {
	s := "\n"
	s += fmt.Sprintf(strings.Repeat("-", 50) + "\n")
	if debug {
		s += fmt.Sprintf("HANDLE: NORMAL\n")
	}
	s += fmt.Sprintf("PID: %d\n", pid)

	exe, err := getExecutablePath(h)
	if err != nil {
		if debug {
			fmt.Printf("%v\n", err)
		}
	} else {
		s += fmt.Sprintf("PATH: %s\n", exe)
	}

	if infolevel >= 3 {
		cmdLine, currentDirectory, envVars, err := getProcessMemoryInfos(h)
		if err != nil {
			if debug {
				fmt.Printf("%v\n", err)
			}
		} else {
			s += fmt.Sprintf("CMDLINE: %s\n", cmdLine)
			s += fmt.Sprintf("WORKDIR: %s\n", currentDirectory)
			if len(envVars) > 0 && getenv {
				s += fmt.Sprintf("ENV:\n")
				for key, value := range envVars {
					s += fmt.Sprintf("- %s: %s\n", key, value)
				}
			}
		}
	}

	t, err := getToken(h)
	if err != nil {
		//
	} else {
		defer t.Close()

		user, err := getProcessOwner(t)
		if err != nil {
			if debug {
				fmt.Printf("%v\n", err)
			}
		} else {
			s += fmt.Sprintf("USER: %s\n", user)
		}

		IsElevated := t.IsElevated()
		s += fmt.Sprintf("ELEVATED (UAC): ")
		if IsElevated {
			s += fmt.Sprintf("true\n")
		} else {
			s += fmt.Sprintf("false\n")
		}

		IsRestricted, err := t.IsRestricted()
		if err != nil {
			if debug {
				fmt.Printf("%v\n", err)
			}
		} else {
			s += fmt.Sprintf("RESTRICTED: ")
			if IsRestricted {
				s += fmt.Sprintf("true\n")
			} else {
				s += fmt.Sprintf("false\n")
			}
		}

		il, err := getIntegrityLevel(t)
		if err != nil {
			if debug {
				fmt.Printf("%v\n", err)
			}
		} else {
			s += fmt.Sprintf("INTEGRITY: %s\n", strings.ToLower(il))
		}

		privs, err := getAccessRights(t)
		if err != nil {
			if debug {
				fmt.Printf("%v\n", err)
			}
		} else {
			if len(privs) > 0 {
				s += fmt.Sprintf("PRIVILEGES:\n")
				for priv, statusbool := range privs {
					status := "false"
					if statusbool {
						status = "true"
					}
					s += fmt.Sprintf("- %s: %s\n", priv, status)
				}
			}
		}
	}

	var session uint32
	err = windows.ProcessIdToSessionId(uint32(pid), &session)
	if err != nil {
		if debug {
			fmt.Printf("%v\n", err)
		}
	} else {
		s += fmt.Sprintf("SESSION: %d\n", session)
	}

	chain, err := resolveProcessChain(pid)
	if err == nil {
		s += fmt.Sprintf("PARENTS: %s\n", chain)
	}

	fmt.Print(s)
}

func getInfo(pid uint32) {
	infolevel, h, err := getHandle(pid)
	if err != nil {
		s := "\n"
		s += fmt.Sprintf(strings.Repeat("-", 50) + "\n")
		if debug {
			s += fmt.Sprintf("HANDLE: NONE\n")
		}
		s += fmt.Sprintf("PID: %d\n", pid)
		exe, err := getExecutable(pid)
		if err != nil {
			if debug {
				fmt.Printf("%v\n", err)
			}
		} else {
			s += fmt.Sprintf("EXE: %s\n", exe)
		}

		chain, err := resolveProcessChain(pid)
		if err == nil {
			s += fmt.Sprintf("PARENTS: %s\n", chain)
		}
		fmt.Print(s)
	}
	if infolevel == 1 {
		displayLimited(pid, h)
	} else {
		display(pid, h, infolevel)
	}
	defer windows.CloseHandle(h)
}

func main() {
	if len(os.Args) == 2 {
		switch os.Args[1] {
		case "getenv":
			getenv = true
		default:
			return
		}
	}
	var oldpids []uint32
	refreshProcessList()
	for {
		pids := getPids()
		for _, pid := range pids {
			if pid == uint32(0) {
				continue
			}
			if !slices.Contains(oldpids, pid) {
				go getInfo(pid)
			}
		}
		oldpids = pids
		time.Sleep(time.Duration(interval1) * time.Millisecond)
	}
}
