#!/bin/bash
cd -- "$(dirname -- "$0")"
docker run --rm -it -v "$(pwd):/workspace/" gobuilder:latest bash /workspace/build.sh
